var artistApp = angular.module('artistApp', [ 'ngRoute' ]);

artistApp.config(function($routeProvider) {

	$routeProvider

	.when('/artist', {
		templateUrl : 'artist-home.html',
		controller : 'artistController'
	})

	.when('/album/:artistname', {
		templateUrl : 'album-home.html',
		controller : 'albumController'
	})

	.when('/album', {
		templateUrl : 'album-home.html',
		controller : 'albumController'
	})

	.when('/track/:albumname', {
		templateUrl : 'track-home.html',
		controller : 'trackController'
	});

});

artistApp.controller('handleLinkController', function($scope, $location) {

	$location.path('/artist');

});

artistApp
		.controller(
				'artistController',
				function($scope, $http, $rootScope) {

					$scope.items = {};

					$scope.find = function() {

						if (angular.isUndefined($scope.artistname)
								|| $scope.artistname == null || $scope.artistname == '') {
							
							alert(" Artist name cannot be empty");
							
							$scope.items = {};
							
							return;
						}


						var url = "https://api.spotify.com/v1/search?callback=JSON_CALLBACK&offset=0&limit=20&type=artist&query="
								+ $scope.artistname;

						$http.get(url).success(function(data) {

							$scope.items = data.artists.items;

						});

					};

				});

artistApp
		.controller(
				'albumController',
				function($scope, $routeParams, $http) {

					var url = "https://api.spotify.com/v1/search?callback=JSON_CALLBACK&offset=0&limit=20&type=album&query="
							+ $routeParams.artistname;

					$http.get(url).success(function(data) {

						$scope.items = data.albums.items;

					});

				});

artistApp
		.controller(
				'trackController',
				function($scope, $routeParams, $http) {

					var url = "https://api.spotify.com/v1/search?callback=JSON_CALLBACK&offset=0&limit=20&type=track&query="
							+ $routeParams.albumname;

					$http.get(url).success(function(data) {

						$scope.items = data.tracks.items;

					});

				});